# ---------------------------------------------------------------------------- #
# Copyright 2018-2021, OpenNebula Project, OpenNebula Systems                  #
#                                                                              #
# Licensed under the Apache License, Version 2.0 (the "License"); you may      #
# not use this file except in compliance with the License. You may obtain      #
# a copy of the License at                                                     #
#                                                                              #
# http://www.apache.org/licenses/LICENSE-2.0                                   #
#                                                                              #
# Unless required by applicable law or agreed to in writing, software          #
# distributed under the License is distributed on an "AS IS" BASIS,            #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.     #
# See the License for the specific language governing permissions and          #
# limitations under the License.                                               #
# ---------------------------------------------------------------------------- #

# Important notes #############################################################
#
# This appliance does nothing, it's just a template to create new ones.
#
# Important notes #############################################################


# List of contextualization parameters
ONE_SERVICE_PARAMS=(
    'ONEAPP_PARAM' 'configure' 'A parameter' 'M|boolean'
)


### Appliance metadata ########################################################

ONE_SERVICE_NAME='Service Template'
ONE_SERVICE_VERSION=0.1
ONE_SERVICE_BUILD=$(date +%s)
ONE_SERVICE_SHORT_DESCRIPTION='Appliance with nothing'
ONE_SERVICE_DESCRIPTION=$(cat <<EOF
Appliance which does nothing.

Initial configuration can be customized via parameters:

$(params2md 'configure')

EOF
)



### Contextualization defaults ################################################

ONEAPP_PARAM='FOO'



### Globals ###################################################################

DEP_PKGS="
    ca-certificates
    coreutils
    curl
    jq
    openssh-server
    openssl
    pwgen
"


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


#
# service implementation
#

service_cleanup()
{
    :
}

service_install()
{
    # packages
    install_pkgs ${DEP_PKGS}

    # service metadata
    create_one_service_metadata

    # cleanup
    postinstall_cleanup

    msg info "INSTALLATION FINISHED"

    return 0
}

service_configure()
{

    msg info "CONFIGURATION FINISHED"

    return 0
}

service_bootstrap()
{

    setup_onegate
    msg info "BOOTSTRAP FINISHED"

    return 0
}



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


#
# functions
#

setup_onegate()
{
    _onegate_vm_id=$(onegate vm show --json | jq -cr ".VM.ID" || true)

    if ! echo "$_onegate_vm_id" | grep -q '^[0-9]\+$' ; then
        msg info "No OneFlow support"
        return 0
    fi

    msg info "Set OneFlow service data..."

    msg info "OneFlow service value: ONEGATE_FOO"
    onegate vm update --data ONEGATE_FOO="$ONEAPP_PARAM"
}

postinstall_cleanup()
{
    msg info "Delete cache and stored packages"
    yum clean all
    rm -rf /var/cache/yum
}

install_pkgs()
{
    msg info "Enable EPEL repository"
    if ! yum install -y --setopt=skip_missing_names_on_install=False epel-release ; then
        msg error "Failed to enable EPEL repository"
        exit 1
    fi

    msg info "Install required packages"
    if ! yum install -y --setopt=skip_missing_names_on_install=False "${@}" ; then
        msg error "Package(s) installation failed"
        exit 1
    fi
}
