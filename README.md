# OpenNebula appliance template

This is a minimalist appliance template based on Kubernetes 1.21 structure.

## Setup

On a virtual machine which will be used as base for the service:

1. clone or copy this repository somewhere on the VM
2. execute `./setup` to copy files and directory to their destination
3. execute `/etc/one-appliance/service install` to execute the prime
   installation of tools used during the `configure` at VM boot
